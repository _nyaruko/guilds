package io.nyaruko.guilds.commands;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;

import io.nyaruko.guilds.Guilds;
import io.nyaruko.guilds.GuildsAPI;
import io.nyaruko.guilds.SQLManager;

public class Claim implements CommandExecutor {
	
	public Claim() {}
	@Override
	public CommandResult execute(CommandSource arg0, CommandContext arg1) throws CommandException {
		if(arg0 instanceof Player){
			Player p = (Player) arg0;
			int guildID = GuildsAPI.getMemberGuildID(p.getUniqueId());
			String world = p.getWorld().getUniqueId().toString();
			
			int xChunk = GuildsAPI.ordinateToChunkValue(p.getLocation().getBlockX());
			int zChunk = GuildsAPI.ordinateToChunkValue(p.getLocation().getBlockZ());
			
			if(arg1.getOne("shape").orElse(null) != null){//Check if a claim shape was specified
				if(arg1.getOne("shape").get().toString().equals("square")){//Radius claim as a square
					int radius = Integer.parseInt(arg1.getOne("radius").get().toString());
					if(radius <= 20){//TODO Configuration
						boolean allClear = true;
						int lowX = (int)(xChunk - Math.floor(radius / 2));
						int highX = (int)(xChunk + Math.floor(radius / 2));
						int lowZ = (int)(zChunk - Math.floor(radius / 2));
						int highZ = (int)(zChunk + Math.floor(radius / 2));
						
						if(GuildsAPI.guildHasFreeInfluence(guildID, (int)Math.pow(radius, 2))){
							for(int i0 = lowX; i0 <= highX; i0++){
								for(int i1 = lowZ; i1 <= highZ; i1++){
									if (!GuildsAPI.claimCheck(xChunk, zChunk, world, p, guildID)){
										allClear = false;
										p.sendMessage(Text.of(i0 + "," + i1));
									}
								}
							}
						
							if(allClear){
								for(int i0 = lowX; i0 <= highX; i0++){
									for(int i1 = lowZ; i1 <= highZ; i1++){
										if (GuildsAPI.claimCheck(xChunk, zChunk, world, p, guildID)){
											claimGrounds(guildID, xChunk, zChunk, world);
										}
										p.sendMessage(Text.of(i0 + "," + i1));
									}
								}
								p.sendMessage(Text.builder("Successfully claimed").color(TextColors.RED).build());
							}else{
								p.sendMessage(Text.builder("Claim failed: Radius overlaps with existing claims.").color(TextColors.RED).build());
							}
						}else{
							p.sendMessage(Text.builder("You do not have enough influence to claim this land.").color(TextColors.RED).build());
						}
					}
					
					p.sendMessage(Text.of(arg1.getOne("radius").get().toString() + " circle size"));
				}else if(arg1.getOne("shape").get().toString().equals("circle")){
					p.sendMessage(Text.of("what is circle"));
				}
			}
			if (GuildsAPI.claimCheck(xChunk, zChunk, world, p, guildID)){
				claimGrounds(guildID, xChunk, zChunk, world);
				p.sendMessage(Text.builder("["+ xChunk + ", " + zChunk + "] ").color(TextColors.GREEN)
					 .append(Text.builder("You successfully claimed land for your guild!").color(TextColors.GOLD).build()).build());
			}
		}
		return CommandResult.success();
	}
	
	private void claimGrounds(int guildID, int xChunk, int zChunk, String World) {
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT * FROM Guilds WHERE id = '" + guildID + "'");
			if(rs.next()){
				int newGrounds = rs.getInt("Grounds") + 1;
				rs = SQLManager.runSQLQuery("SELECT * FROM Grounds WHERE ChunkX = '" + xChunk + "' AND ChunkZ = '" + zChunk + "' AND World = '" + World + "'");
				if (rs.next()){
					int takenID = rs.getInt("GuildID");
					Guilds.logger.info(takenID + "");
					rs = SQLManager.runSQLQuery("SELECT * FROM Guilds WHERE id = '" + takenID + "'");
					if(rs.next()){
						int prevGrounds = rs.getInt("Grounds") - 1;
					
						SQLManager.runSQLAction("UPDATE Guilds SET Grounds = '" + prevGrounds + "' WHERE id ='" + takenID + "'"); //Updates count of grounds in guild that have been taken over
						SQLManager.runSQLAction("UPDATE Grounds SET GuildID = '" + guildID + "' WHERE ChunkX = '" + xChunk + "' AND ChunkZ = '" + zChunk + "' AND World = '" + World + "'");
					}
				}else{
					SQLManager.runSQLAction("INSERT INTO Grounds (GuildID, ChunkX, ChunkZ, World) VALUES ('" + guildID + "', '" + xChunk +"', '" + zChunk +"', '" + World + "')");
				}
				SQLManager.runSQLAction("UPDATE Guilds SET Grounds = '" + newGrounds + "' WHERE id ='" + guildID + "'");
			}
			
		} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
		}
	}
}
