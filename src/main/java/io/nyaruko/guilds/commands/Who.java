package io.nyaruko.guilds.commands;

import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import io.nyaruko.guilds.GuildsAPI;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;

public class Who implements CommandExecutor{
	
	public Who() {}

	@Override
	public CommandResult execute(CommandSource arg0, CommandContext arg1) throws CommandException {
		if(arg0 instanceof Player){
			Player p = (Player) arg0;
			if(GuildsAPI.playerInGuild(p.getUniqueId())){
				String guild = GuildsAPI.getGuildName(p);//TODO different gets for username and guild name
				String description = GuildsAPI.getGuildDescription(guild);
				String influenceReading = GuildsAPI.getGuildInfluence(guild) + " | " + GuildsAPI.getGuildMaxInfluence(guild);
				int grounds = GuildsAPI.getGuildGroundsByName(guild);
				String onlineMembers = GuildsAPI.getActiveMembers(guild);
				String offlineMembers = GuildsAPI.getDormantMembers(guild);
				
				p.sendMessage(Text.builder("--------------{").color(TextColors.GOLD)
						 .append(Text.builder(guild).color(TextColors.LIGHT_PURPLE)
						 .append(Text.builder("}--------------").color(TextColors.GOLD)
						 .build()).build()).build());
				p.sendMessage(Text.builder("Description: ").color(TextColors.GOLD).append(Text.builder(description).color(TextColors.WHITE).build()).build()); //Description
				p.sendMessage(Text.of(""));
				p.sendMessage(Text.builder("Influence: " ).color(TextColors.GOLD).append(Text.builder(influenceReading).color(TextColors.WHITE).build()).build()); //Influence
				p.sendMessage(Text.of(""));
				p.sendMessage(Text.builder("Grounds: " ).color(TextColors.GOLD).append(Text.builder(String.valueOf(grounds)).color(TextColors.WHITE).build()).build()); //Claimed land
				p.sendMessage(Text.of(""));
				p.sendMessage(Text.builder("Online Members: ").color(TextColors.GOLD).append(Text.builder(onlineMembers).color(TextColors.WHITE).build()).build()); //Members online
				p.sendMessage(Text.of(""));
				p.sendMessage(Text.builder("Dormant Members: ").color(TextColors.GOLD).append(Text.builder(offlineMembers).color(TextColors.WHITE).build()).build()); //Members offline
			}else{
				p.sendMessage(Text.builder("You're not a member of a guild! Do /g create to make one of your own.").color(TextColors.RED).build()); //Members offline//TODO check perms for that second sentence
			}
		}
		return CommandResult.success();
	}
}
