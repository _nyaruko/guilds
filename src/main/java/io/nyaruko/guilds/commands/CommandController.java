package io.nyaruko.guilds.commands;

import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;

public class CommandController implements CommandExecutor{

	@Override
	public CommandResult execute(CommandSource arg0, CommandContext arg1) throws CommandException {
		Sponge.getCommandManager().process(arg0, "g who");//TODO Configuration
		return CommandResult.success();
	}
}
