package io.nyaruko.guilds.commands;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColor;
import org.spongepowered.api.text.format.TextColors;

import io.nyaruko.guilds.Guilds;
import io.nyaruko.guilds.GuildsAPI;
import io.nyaruko.guilds.SQLManager;

public class ListCommand implements CommandExecutor {

	public ListCommand() {}

	@Override
	public CommandResult execute(CommandSource arg0, CommandContext arg1) throws CommandException {
		//TODO consider using/making something like mmonkey's pagination service
		if(!arg1.getOne("page").isPresent()){
			getGuildsByGrounds(1, arg0);
		}else{
			try{
				getGuildsByGrounds(Integer.parseInt(arg1.getOne("page").get().toString()), arg0);
			}catch(Exception e){
				Guilds.logger.error(e.toString());
				arg0.sendMessage(Text.of("That is not a valid page number!"));
			}
		}
		
		return CommandResult.success();
	}

	private void getGuildsByGrounds(int page, CommandSource arg0) {
		int totalPages=0;
		if (page < 1){
			arg0.sendMessage(Text.builder("You must enter a page number above or equal to 1.").color(TextColors.RED).build());
			return;
		}
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT COUNT(*) AS rowcount FROM Guilds WHERE id != '1'");
			if (rs.next()){
				totalPages = (int) Math.ceil(rs.getDouble("rowcount")/8);
			}else{
				arg0.sendMessage(Text.builder("There are currently no factions to list.").color(TextColors.GOLD).build());
			}
			
			if(page > totalPages){
				arg0.sendMessage(Text.builder("There are only " + totalPages + " pages!").color(TextColors.RED).build());
				return;
			}
			arg0.sendMessage(Text.builder("--------------------{Page " + page + "/" + totalPages + "}--------------------").color(TextColors.GOLD).build());
			
			rs = SQLManager.runSQLQuery("SELECT * FROM Guilds WHERE id != '1' ORDER BY Grounds DESC LIMIT 8 OFFSET " + (page*8 - 8));
			while(rs.next()){
				int guildID = rs.getInt("id");
				String guildName = rs.getString("Title");
				String influenceGroundsString= GuildsAPI.getGuildInfluence(guildName) + "/" + GuildsAPI.getGuildGroundsByName(guildName) + "/" + GuildsAPI.getGuildMaxInfluence(guildName);
				TextColor groundsColor = TextColors.GRAY;
				
				if(GuildsAPI.getGuildInfluence(guildName) < GuildsAPI.getGuildGroundsByName(guildName)){
					groundsColor = TextColors.RED;
				}else{
					groundsColor = TextColors.GREEN;
				}
				
				arg0.sendMessage(Text.builder(guildName).color(GuildsAPI.getGuildRelationColor(arg0.getName(), guildID))
						.append(Text.builder(" - ").color(TextColors.GOLD)
						.append(Text.builder(influenceGroundsString).color(groundsColor)
						.append(Text.builder(" - ").color(TextColors.GOLD)
						.build()).build()).build()).build());
			}
			
			arg0.sendMessage(Text.builder("Guild - Influence/Grounds/Max - Members Online/Offline").color(TextColors.GOLD).build());
		}  catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
			arg0.sendMessage(Text.of("A fault occured in collecting this information, please report this to an administrator."));
		}
		
		
	}
}
