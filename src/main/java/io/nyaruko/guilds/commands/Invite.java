package io.nyaruko.guilds.commands;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import io.nyaruko.guilds.Guilds;
import io.nyaruko.guilds.GuildsAPI;
import io.nyaruko.guilds.SQLManager;
import io.nyaruko.guilds.models.Rank;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;

public class Invite implements CommandExecutor {

	public Invite() {}

	@Override
	public CommandResult execute(CommandSource arg0, CommandContext arg1) throws CommandException {
		if(arg0 instanceof Player){
			Player sourcePlayer = (Player) arg0;
			String sourceName = sourcePlayer.getName();
			Player targetPlayer = arg1.<Player>getOne("player").get();
	        String name = targetPlayer.getName();
	        if (GuildsAPI.getMemberRankByName(sourceName).value >= Rank.MOD.value){
	        	if(!sourceName.equals(name)){
	        		if(GuildsAPI.getMemberGuildID(sourcePlayer.getUniqueId()) != GuildsAPI.getMemberGuildID(targetPlayer.getUniqueId())){
	        			createInvite(sourceName, name);
	        			sourcePlayer.sendMessage(Text.builder("You have invited " + name + " to your guild.").color(TextColors.GOLD).build());
	        			targetPlayer.sendMessage(Text.builder("You have been invited to the guild '" + GuildsAPI.getGuildName(sourcePlayer) + "' by " + sourcePlayer.getName() + ".").color(TextColors.GOLD).build());
	        		}else{
		        		sourcePlayer.sendMessage(Text.builder(name + " is already in your guild.").color(TextColors.RED).build());
	        		}
	        	}else{
	        		sourcePlayer.sendMessage(Text.builder("You can't invite yourself to your own guild!").color(TextColors.RED).build());
	        	}
	        }else{
	        	sourcePlayer.sendMessage(Text.builder("You must be a mod or leader of your guild to invite players.").color(TextColors.RED).build());
	        }
		}
        return CommandResult.success();
	}
	
	public Boolean createInvite(String source, String target) {
		String GuildID = null;
		String UserID = null;
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT * FROM Players WHERE Username = '" + source + "'");
			rs.next();
			GuildID = rs.getString("GuildID");
			rs = SQLManager.runSQLQuery("SELECT * FROM Players WHERE Username = '" + target + "'");
			rs.next();
			UserID = rs.getString("id");
			
			if(GuildID == "1"){ //Check that the user is not a member of the default guild.
				//return false;
			}
			
			SQLManager.runSQLAction("INSERT INTO Invites (GuildID, UserID) VALUES ('" + GuildID + "', '" + UserID + "')");
			Guilds.logger.info("An invite to your guild was sent to '" + target + "'!");
			return true;
		} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
			return false;
		}
	}
}
