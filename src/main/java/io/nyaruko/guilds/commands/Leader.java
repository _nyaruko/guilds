package io.nyaruko.guilds.commands;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import io.nyaruko.guilds.Guilds;
import io.nyaruko.guilds.SQLManager;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;

public class Leader implements CommandExecutor {
	
	public Leader() {}

	@Override
	public CommandResult execute(CommandSource arg0, CommandContext arg1) throws CommandException {
		String name;
		Player oldLeader = Sponge.getServer().getPlayer(arg0.getName()).get();
		Player player = arg1.<Player>getOne("player").orElse(null);
		if (player != null) {
		name = player.getName();
		}else{
			Guilds.logger.info("");
			name = checkPlayerExists(arg1.getOne("playerStr").orElse(null).toString());
		}
		
		if (name != null){
			if(checkSourceIsLeader(arg0.getName())){
				if (playersInSameGuild(name, arg0.getName())){
					setLeader(name, arg0.getName());
					if(player != null){
					player.sendMessage(Text.builder("You have been promoted to leader of your guild!").color(TextColors.GOLD).build());
					}
					oldLeader.sendMessage(Text.builder("You have promoted " + name + " to leader of your guild.").color(TextColors.GOLD).build());
				}else{
					oldLeader.sendMessage(Text.builder("The player specified is not a member of your guild.").color(TextColors.RED).build());
				}
			}else{
				oldLeader.sendMessage(Text.builder("You must be the leader of your guild to promote a new leader.").color(TextColors.RED).build());
			}
		}else{
			oldLeader.sendMessage(Text.builder("Failed to get offline player, a full name must be entered for offline players.").color(TextColors.RED).build());
		}
		return CommandResult.success();
	}

	private String checkPlayerExists(String name) {
		if (name == null){
			return null;
		}
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT * FROM Players WHERE Username = '" + name + "'");
			
			if(rs.next()){
				return rs.getString("Username");
			}else{
			return null;
			}
			
			} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
			return null;
		}
	}

	private boolean playersInSameGuild(String name, String oldLeader) {
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT * FROM Players WHERE Username = '" + name + "'");
			rs.next();
			
			int targetsGuild = rs.getInt("GuildID");
			
			rs = SQLManager.runSQLQuery("SELECT * FROM Players WHERE Username = '" + oldLeader + "'");
			rs.next();
			
			int leadersGuild = rs.getInt("GuildID");
			
			if(targetsGuild == leadersGuild){ 
			return true;
			}else{
				return false;
			}
		} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
			return false;
		}
	}

	private void setLeader(String name, String oldLeader) {
		try {
			SQLManager.runSQLAction("UPDATE Players SET Rank = '2' WHERE Username ='" + name + "'"); //Make a new leader
			SQLManager.runSQLAction("UPDATE Players SET Rank = '0' WHERE Username ='" + oldLeader + "'"); //Demote old leader, there can only be one!
			
			} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
		}
	}

	private boolean checkSourceIsLeader(String name) {
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT * FROM Players WHERE Username = '" + name + "'");
			rs.next();
			if(rs.getInt("Rank") == 2){ //If rank is 2 then they are the leader, though not for long.
			return true;
			}else{
				return false;
			}
		} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
			return false;
		}
	}

}
