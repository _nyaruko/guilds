package io.nyaruko.guilds.commands;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.Text.Builder;
import org.spongepowered.api.text.format.TextColor;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;

import io.nyaruko.guilds.GuildsAPI;
import io.nyaruko.guilds.SQLManager;
//TODO add toggle
public class MapCommand implements CommandExecutor {
	int[] guildIDList;
	public MapCommand() {}

	@Override
	public CommandResult execute(CommandSource arg0, CommandContext arg1) throws CommandException {
		Player p = (Player) arg0;
		String world = Sponge.getServer().getPlayer(p.getName()).get().getWorld().getUniqueId().toString();
		int xChunk = GuildsAPI.ordinateToChunkValue(p.getLocation().getBlockX());
		int zChunk = GuildsAPI.ordinateToChunkValue(p.getLocation().getBlockZ());
		int currentChunkOwnerID = GuildsAPI.getChunkOwnerID(xChunk, zChunk, world);
		Map<Integer, TextColor> colourCache = new HashMap<>();//For optimisation
		Text mapHead;
		mapHead = Text.builder("---------------{")
				.color(TextColors.GOLD)
					.append(Text.builder("[" + xChunk + ", " + zChunk + "]")
					.color(TextColors.GREEN)
						.append(Text.builder(GuildsAPI.getGuildTitleFromID(currentChunkOwnerID))
						.color(GuildsAPI.getGuildRelationColor(p.getName(), currentChunkOwnerID))
							.append(Text.builder("}---------------")
							.color(TextColors.GOLD)
							.build())
						.build())
					.build())
				.build();
		Text[] map = populateMap();
		
		guildIDList = setupGuildArray(); //Max 66 different guilds have symbols in the map, second value stores the symbol for the guild who owns it
		
		Map<String, Integer>chunkOwners = new HashMap<>();
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT GuildID, ChunkX, ChunkZ FROM Grounds WHERE ChunkX >= " + (xChunk - 3) + " AND ChunkX <= " + (xChunk + 3) +
					" AND ChunkZ >= " + (zChunk - 20) + " AND ChunkZ <= " + (zChunk + 20) + " AND World = '" + world + "'");
			while(rs.next()){
				chunkOwners.put(rs.getInt("ChunkX")+","+rs.getInt("ChunkZ"), rs.getInt("GuildID"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		for(int i0 = -3; i0 <= 3; i0++){
			for(int i1 = -20; i1 <= 20; i1++){//TODO Configuration
				if(i1 < -17 && i0 < 0){
					continue;
				}
				String key = i0 + "," + i1;
				int id = chunkOwners.containsKey(key) ? chunkOwners.get(key) : 1;//get chunk with offset from center
				if (i0 == 0 && i1 == 0){
					map[i0+3] = Text.builder().append(map[i0+3]).append(Text.builder("+").color(TextColors.GOLD).build()).build();
					continue;
				}
				if(id != 1){
					String symbol = getArrayGuildSymbol(id, false);//TODO This is wrong and bad
					TextColor color;
					if(colourCache.containsKey(id)){//Cache check
						color = colourCache.get(id);
					}else{
						color = GuildsAPI.getGuildRelationColor(p.getName(), id);
						colourCache.put(id, color);
					}
					map[i0+3] = Text.builder().append(map[i0+3]).append(Text.builder(symbol).color(color).build()).build();
				}else{
					map[i0+3] = Text.builder().append(map[i0+3]).append(Text.builder("-").color(TextColors.GRAY).build()).build();
				}
				
			}
		}
		
		ArrayList<Text> compass = generateCompassText(p);
		//Setup and send the map header and contents
		Builder fullMap = Text.builder().append(mapHead).append(Text.NEW_LINE);
		for(int i = 0; i < 7; i++){
			if(i < 3){
				fullMap.append(compass.get(i));
			}
			fullMap.append(map[i]);
			if(i != 6){
				fullMap.append(Text.NEW_LINE);
			}
		}
		p.sendMessage(fullMap.build());
		
		
		//Setup and send the key for the map symbols
		String guildKey = "";
		for (int i = 0; i < 37; i++){
			if(i == 0){
				guildKey += "-: " + GuildsAPI.getDefaultGuildData()[0];
				continue;
			}
			if (guildIDList[i] == -1){
				break;
			}
			guildKey += " | " +(getArrayGuildSymbol(i, true) + " - " + GuildsAPI.getGuildTitleFromID(guildIDList[i]));
		}
		p.sendMessage(Text.builder(guildKey).color(TextColors.GOLD).build());
		
		return CommandResult.success();
	}
	
	private int[] setupGuildArray() {//Initialises an array of size 65 that is full of -1s
		int[] array = new int[65];
		for (int i = 0; i < 65; i++){
			array[i]=-1;
		}
		return array;
	}
	
	private Direction angleToDirection(double angle){
		double degrees = (angle - 180) % 360 ;
		if (degrees < 0)
			degrees += 360;
		
		if ((0 <= degrees && degrees < 22.5) || (337.5 <= degrees && degrees < 360.0))
			return Direction.NORTH;
		else if (22.5 <= degrees && degrees < 67.5)
			return Direction.NORTHEAST;
		else if (67.5 <= degrees && degrees < 112.5)
			return Direction.EAST;
		else if (112.5 <= degrees && degrees < 157.5)
			return Direction.SOUTHEAST;
		else if (157.5 <= degrees && degrees < 202.5)
			return Direction.SOUTH;
		else if (202.5 <= degrees && degrees < 247.5)
			return Direction.SOUTHWEST;
		else if (247.5 <= degrees && degrees < 292.5)
			return Direction.WEST;
		else if (292.5 <= degrees && degrees < 337.5)
			return Direction.NORTHWEST;
		else
			return null;
		
	}

	private Text[] populateMap() {
		Text[] map = new Text[7];
		for (int i=0; i < 7; i++){
			map[i] = Text.of("");
		}
		return map;
	}
	
	private ArrayList<Text> generateCompassText(Player p){
		Direction d = angleToDirection(p.getHeadRotation().getY());
		TextColor y = TextColors.RED;//TODO Configuration
		TextColor n = TextColors.GOLD;
		ArrayList<Text> compass = new ArrayList<>();
		compass.add(Text.builder("\\")
					.color(d==Direction.NORTHWEST ? y : n)
						.append(Text.builder("N")
						.color(d==Direction.NORTH ? y : n)
							.append(Text.builder("/")
							.color(d==Direction.NORTHEAST ? y : n)
							.build())
						.build())
					.build());
		compass.add(Text.builder("W")
				.color(d==Direction.WEST ? y : n)
				.append(Text.builder("X")
				.color(n)
					.append(Text.builder("E")
					.color(d==Direction.EAST ? y : n)
					.build())
				.build())
			.build());
		compass.add(Text.builder("/")
				.color(d==Direction.SOUTHWEST ? y : n)
				.append(Text.builder("S")
				.color(d==Direction.SOUTH ? y : n)
					.append(Text.builder("\\")
					.color(d==Direction.SOUTHEAST ? y : n)
					.build())
				.build())
			.build());
		
		return compass;
	}
	
	private String getArrayGuildSymbol(int GuildID, boolean guildIDIsArrayPos){
		int arrayPos = -1;
		int nullAt = -1;
		if(!guildIDIsArrayPos){
		for (int i = 0; i < 65; i++){
			if (guildIDList[i] == GuildID){
				arrayPos = i;
			}
			if (guildIDList[i] == -1){
				nullAt = i;
				break;
			}
		}
		
		if (arrayPos == -1){
			if (nullAt != -1){
				guildIDList[nullAt] = GuildID;
				arrayPos = nullAt;
			}else{
				return "Z";
			}
		}
		}else{
			arrayPos = GuildID;
		}
		
		return String.valueOf("#@&£$^%=?ABCDFGHIJKLMOPQRTUVYZabcdefghijklmnopqrstuvwxyz0123456789".charAt(arrayPos));
	}
}
