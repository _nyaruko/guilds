package io.nyaruko.guilds.commands;

import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import io.nyaruko.guilds.GuildsAPI;
import io.nyaruko.guilds.models.Rank;

import java.util.UUID;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;

public class Description implements CommandExecutor {
	
	public Description() {}

	@Override
	public CommandResult execute(CommandSource arg0, CommandContext arg1) throws CommandException {
		if(arg0 instanceof Player){
			UUID p = ((Player) arg0).getUniqueId();
			if(!GuildsAPI.playerInGuild(p)){
				arg0.sendMessage(Text.builder("You need to be in a guild to do this.").color(TextColors.RED).build());
			}else if(GuildsAPI.getMemberRankByUUID(p).value < Rank.MOD.value){
				arg0.sendMessage(Text.builder("You need to be at least a mod to change your guild description.").color(TextColors.RED).build());
			}else{
				int GuildID = GuildsAPI.getMemberGuildID(p); 
				GuildsAPI.setGuildDescription(GuildID, arg1.getOne("description").get().toString(), true);
			}
			
		}
		return CommandResult.success();
	}
}
