package io.nyaruko.guilds.commands;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.spongepowered.api.text.Text;
import io.nyaruko.guilds.Guilds;
import io.nyaruko.guilds.SQLManager;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;

public class Enemy implements CommandExecutor {
	public Enemy() {}

	@Override
	public CommandResult execute(CommandSource arg0, CommandContext arg1) throws CommandException {
		if (arg1.getOne("title").orElse(null) != null){
			int targetGuild = getGuildIDFromTitle(arg1.getOne("title").get().toString());
			Guilds.logger.info(targetGuild + "");
			if (targetGuild > 1){
				if(checkSourceIsMod(arg0.getName())){
					int sourceGuildID = getGuildID(arg0.getName()); 
					makeEnemy(sourceGuildID, targetGuild);
					//alertOnlinePlayers(sourceGuildID, targetGuild);
				}else{
					arg0.sendMessage(Text.of("You must be at least a mod of your guild to declare enemies."));
				}
			}else{
				arg0.sendMessage(Text.of("The guild title you have entered is invalid."));
			}
		}else{
			arg0.sendMessage(Text.of("You must enter the title of the guild you wish to make an enemy."));
		}
		return CommandResult.success();
	}
	
	private int getGuildIDFromTitle(String title) {
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT * FROM Guilds WHERE Title = '" + title + "'");
			if(rs.next()){
				return rs.getInt("id");
			}else{
				return 0;
			}
		}catch(SQLException e){
			Guilds.logger.error("A database error has occured: \n" + e);
			return -1;
		}
	}
	
	/*
	private void alertOnlinePlayers(int sourceGuildID, int targetGuild) {
		try {
				ResultSet rs = SQLManager.runSQLQuery("SELECT * FROM Guilds WHERE Title = '" + guild + "'");
				rs.next();
				rs = SQLManager.runSQLQuery("SELECT * FROM Players WHERE GuildID = '" + rs.getInt("id") + "' OR GuildID = '" + targetGuild + "'");
				while(rs.next()){
					Player member = game.getServer().getPlayer(rs.getString("Username")).orElse(null);
					if (member != null){
						if(!member.getName().equals(name)){
							member.sendMessage(Text.builder(name + " has joined your guild.").color(TextColors.GOLD).build());
						}
					}
				}
				
			} catch (SQLException e) {
				Main.logger.error("A database error has occured: \n" + e);
			}
		
	}*/

	private int getGuildID(String name){
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT * FROM Players WHERE Username = '" + name + "'");
			rs.next();
			return rs.getInt("GuildID");
		}catch(SQLException e){
			Guilds.logger.error("A database error has occured: \n" + e);
			return -1;
		}
	}
	
	private boolean checkSourceIsMod(String name) {
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT * FROM Players WHERE Username = '" + name + "'");
			rs.next();
			if(rs.getInt("Rank") >= 1){ //If rank is 2 then they are the leader, though not for long.
			return true;
			}else{
				return false;
			}
		} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
			return false;
		}
	}
	
	public void makeEnemy(int sourceGuild, int targetGuild){
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT * FROM Relations WHERE (GuildIDA = '" + sourceGuild + "' AND GuildIDB = '" + targetGuild +
					"') OR (GuildIDB = '" + sourceGuild + "' AND GuildIDA = '" + targetGuild + "')");
			
			if(!rs.next()){
				SQLManager.runSQLAction("INSERT INTO Relations (GuildIDA, GuildIDB, Relation) VALUES ('" + sourceGuild + "', '" + targetGuild +"', '2')");
				Guilds.logger.info("got here");
			}else{
				SQLManager.runSQLAction("UPDATE Relations SET Relation = '2' WHERE (GuildIDA = '" + sourceGuild + "' AND GuildIDB = '" + targetGuild +
					"') OR (GuildIDB = '" + sourceGuild + "' AND GuildIDA = '" + targetGuild + "')");
				Guilds.logger.info("althere");
			}
			
			} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
		}
	}
}