package io.nyaruko.guilds.commands;

import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import io.nyaruko.guilds.GuildsAPI;
import io.nyaruko.guilds.models.Rank;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;

public class Leave implements CommandExecutor {

	public Leave() {}

	// TODO leader leaving
	@Override
	public CommandResult execute(CommandSource arg0, CommandContext arg1) throws CommandException {
		if(arg0 instanceof Player){
			Player p = (Player) arg0;
			if (GuildsAPI.playerInGuild(p.getUniqueId())) {
				if (GuildsAPI.getMemberRankByName(p.getName()).value < Rank.LEADER.value){
					GuildsAPI.sendGuildMessageExclude(Text.of(p.getName() + "left your guild") ,GuildsAPI.getMemberGuildID(p.getUniqueId()), new String[]{p.getName()});//TODO not a fan of this, needs to be global
					GuildsAPI.setMemberGuild(p.getName(), 1);
					p.sendMessage(Text.builder("You have left your guild.").color(TextColors.GOLD).build());
				}else{
					p.sendMessage(Text.builder("You must either hand over your leadership to another player or disband your guild to leave it as a leader.").color(TextColors.RED).build());
				}
			} else {
				p.sendMessage(Text.builder("You are not in a guild.").color(TextColors.RED).build());
			}
		}
		return CommandResult.success();
	}

	
}
