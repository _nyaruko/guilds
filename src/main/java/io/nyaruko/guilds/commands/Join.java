package io.nyaruko.guilds.commands;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import io.nyaruko.guilds.Guilds;
import io.nyaruko.guilds.GuildsAPI;
import io.nyaruko.guilds.SQLManager;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;

public class Join implements CommandExecutor {

	public Join() {}

	@Override
	public CommandResult execute(CommandSource arg0, CommandContext arg1) throws CommandException {
		String guild = arg1.<String>getOne("title").orElse(null);
		String name = arg0.getName();//TODO efficiency with guild corrected name
		if(isInvited(name, guild)){
			GuildsAPI.setMemberGuild(name, GuildsAPI.getGuildIDByTitle(guild));
			arg0.sendMessage(Text.builder("You have successfully joined guild '" + getCorrectGuildName(guild) + "'!").color(TextColors.GOLD).build()); //correct guild name is used to get proper capitalisation of the guild title
			alertOnlinePlayers(name, getCorrectGuildName(guild));
		} else{
			arg0.sendMessage(Text.builder("You do not have an invite for the guild '" + getCorrectGuildName(guild) + "'.").color(TextColors.RED).build());
		}
		return CommandResult.success();
	}
	
	private void alertOnlinePlayers(String name, String guild) {
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT * FROM Guilds WHERE Title = '" + guild + "'");
			rs.next();
			rs = SQLManager.runSQLQuery("SELECT * FROM Players WHERE GuildID = '" + rs.getInt("id") + "'");
			while(rs.next()){
				Player member = Sponge.getServer().getPlayer(rs.getString("Username")).orElse(null);
				if (member != null){
					if(!member.getName().equals(name)){
						member.sendMessage(Text.builder(name + " has joined your guild.").color(TextColors.GOLD).build());
					}
				}
			}
			
		} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
		}
		
	}

	public String getCorrectGuildName(String name){
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT * FROM Guilds WHERE Title = '" + name + "'");
			rs.next();
			return rs.getString("Title");
		} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
			
			return "Error getting guild name.";
		}
	}

	public boolean isInvited(String user, String title){
		boolean result = false;
		String GuildID = null;
		String UserID = null;
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT * FROM Guilds WHERE Title = '" + title + "'");
			rs.next();
			GuildID = rs.getString("id");
			if(!rs.getBoolean("InviteOnly")){
				return true;
			}
			rs = SQLManager.runSQLQuery("SELECT * FROM Players WHERE Username = '" + user + "'");
			rs.next();
			UserID = rs.getString("id");
			rs = SQLManager.runSQLQuery("SELECT * FROM Invites WHERE GuildID = '" + GuildID + "' AND UserID = '" + UserID + "'");
			if(rs.next()){
				result = true;
			}
			
			return result;
		} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
			return false;
		}
	}
}
