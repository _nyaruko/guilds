package io.nyaruko.guilds.commands;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.text.Text;

import io.nyaruko.guilds.Guilds;
import io.nyaruko.guilds.SQLManager;
import io.nyaruko.guilds.models.Rank;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;

public class Create implements CommandExecutor {

	public Create() {}

	@Override
	public CommandResult execute(CommandSource arg0, CommandContext arg1)throws CommandException {
		String title = arg1.<String>getOne("title").get();
		if (checkTitleAvailability(title)){
			if(title.length() <= 20){ //TODO configability
				createGuild(title);
				setPlayerGuild(arg0.getName(), title, Rank.LEADER);//TODO outputs
			}
		}
		
		return CommandResult.success();
	}

	public void setPlayerGuild(String name, String title, Rank rank) {
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT * FROM Guilds WHERE Title = '" + title + "'");
			
			if(rs.next()){
				String GuildID = rs.getString("id");
				SQLManager.runSQLAction("UPDATE Players SET GuildID = '" + GuildID + "', Rank = '" + rank.value + "' WHERE Username ='" + name + "'");
				Sponge.getServer().getBroadcastChannel().send(Text.of(name + " joined guild " + title + "."));
			}
			} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
		}
	}

	private void createGuild(String title) {
		try {
			SQLManager.runSQLAction("INSERT INTO Guilds (Title, Description) VALUES ('" + title + "', 'No description.')");
			Guilds.logger.info("The guild '" + title + "' was created!");
			
		} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
		}
	}
	

	private boolean checkTitleAvailability(String title) {
		boolean result = false;
		try {
			ResultSet rs;
			
			rs = SQLManager.runSQLQuery("SELECT * FROM Guilds WHERE Title = '" + title + "'");
			if(!rs.next()){
				result = true;
			}
			
			return result;
		} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
			return false;
		}
		
	}

}
