package io.nyaruko.guilds.models;

import org.spongepowered.api.text.format.TextColor;
import org.spongepowered.api.text.format.TextColors;

public enum Relation{
	NEUTRAL(0), TRUCE(1), ENEMY(2), WAR(3), ALLY(4);
	
	int value;
	public TextColor color;
	Relation(int value){
		this.value = value;
	}
	
	/***
	 * Non-case sensitive
	 * @param value
	 */
	Relation(String value){//TODO config load colours
		int val;
		switch(value.toLowerCase()){
			case "ally": val = 4; color = TextColors.GREEN; break;
			case "war": val = 3; color = TextColors.DARK_RED; break;
			case "enemy": val = 2; color = TextColors.RED; break;
			case "truce": val = 1; color = TextColors.DARK_GREEN; break;
			case "neutral": val = 0; color = TextColors.GRAY; break;
			default: val = -1;
		}
		this.value = val;
	}
	
	@Override
	public String toString(){
		switch(value){
			case 4: return "ally";
			case 3: return "war";
			case 2: return "enemy";
			case 1: return "truce";
			case 0: return "neutral";
			default: return "unknown";
		}
	}
}
