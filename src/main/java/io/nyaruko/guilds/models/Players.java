package io.nyaruko.guilds.models;

import java.util.UUID;

public class Players {
	int id;
	UUID uuid;
	String name;
	int guildID;
	int maxInfluence;
	int influence;
	Rank rank;
}
