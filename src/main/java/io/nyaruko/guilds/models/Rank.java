package io.nyaruko.guilds.models;

public enum Rank {
DEFAULT(0), TRAINING(1), MEMBER(2), MOD(3), LEADER(4);
	
	public int value;
	Rank(int value){
		this.value = value;
	}
	
	/***
	 * Non-case sensitive
	 * @param value
	 */
	Rank(String value){
		int val;
		switch(value.toLowerCase()){
			case "leader": val = 4; break;
			case "mod": val = 3; break;
			case "member": val = 2; break;
			case "training": val = 1; break;
			default: val = 0;
		}
		this.value = val;
	}
	
	@Override
	public String toString(){
		switch(value){
			case 4: return "leader";
			case 3: return "mod";
			case 2: return "member";
			case 1: return "training";
			default: return "default";
		}
	}
}
