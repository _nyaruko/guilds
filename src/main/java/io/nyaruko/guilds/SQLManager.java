package io.nyaruko.guilds;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SQLManager {
	public static Connection con = null;
	
	public static void setupDatabase() throws SQLException{
        String user = "guildstest";
        String password = "testpass";
        String url = "jdbc:mysql://178.32.10.184:3306/guildstest?autoReconnect=true";//TODO Configuration
		con = DriverManager.getConnection(url,user,password);
		Guilds.logger.info("Successfully connected to database!");
	}
	
	public static ResultSet runSQLQuery(String sql) throws SQLException{
		ResultSet rs = null;
		try(Statement st = con.createStatement()){
			rs = st.executeQuery(sql);
		}
		return rs;
	}
	
	public static void runSQLAction(String sql) throws SQLException{
		try(Statement st = con.createStatement()){
			st.executeUpdate(sql);
		}
	}
}
