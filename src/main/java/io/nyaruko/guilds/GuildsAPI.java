package io.nyaruko.guilds;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColor;
import org.spongepowered.api.text.format.TextColors;

import io.nyaruko.guilds.models.Rank;

public class GuildsAPI {
	public static String[] getDefaultGuildData(){//TODO return a GuildsModel
		String[] data = new String[]{"Default", "Default"};//TODO get default from config
		ResultSet rs;		
		try {
			rs = SQLManager.runSQLQuery("SELECT * FROM Guilds WHERE id = '1'");
			if(rs.next()){
				data[0] = rs.getString("Title");
				data[1] = rs.getString("Description");	
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return data;
	}
	
	public static String getGuildTitleFromID(int id) {
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT * FROM Guilds WHERE id = '" + id + "'");
			if(rs.next()){
			return rs.getString("Title");
			}else{
				return "Error";
			}
		}catch(SQLException e){
			Guilds.logger.error("A database error has occured: \n" + e);
			return "Error";
		}
	}
	
	/***
	 * Returns the coloured relating to the guild relation between the player's guild and the target guild.
	 * The default colour relations are as such:
	 * Player's own guild - Aqua
	 * Ally - Green
	 * Truce - Dark Green
	 * Enemy - Red
	 * War - Dark Red
	 * Grey - Neutral
	 * Error while getting relation - Yellow
	 * @param playerName
	 * @param targetGuildID
	 * @return The appropriate TextColor for the relation
	 */
	public static TextColor getGuildRelationColor(String playerName, int targetGuildID){
		int sourceGuildID = 1;
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT * FROM Players Where Username = '" + playerName + "'");
			if(rs.next()){
				sourceGuildID = rs.getInt("GuildID");
				if(sourceGuildID == targetGuildID){
					return TextColors.AQUA;//colour if the chunk is owned by your guild
				}
			}
			rs = SQLManager.runSQLQuery("SELECT * FROM Relations WHERE (GuildIDA = '" + sourceGuildID + "' AND GuildIDB = '" + targetGuildID +
					"') OR (GuildIDB = '" + sourceGuildID + "' AND GuildIDA = '" + targetGuildID + "')");
			if(rs.next()){
				if (rs.getInt("Relation") == 1){
					return TextColors.GREEN;
				} else{//TODO Other relation levels
					return TextColors.RED;
				}
			}else{
				return TextColors.YELLOW;//default guild colour
			}
		} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
		}
		return TextColors.YELLOW;
	}
	
	public static int getGuildInfluence(String guildName){
		int totalInfluence = 0;
		
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT * FROM Guilds WHERE Title = '" + guildName + "'");
			if(rs.next()){
				int id = rs.getInt("id");
				rs = SQLManager.runSQLQuery("SELECT * FROM Players WHERE GuildID = '" + id + "'");
				while(rs.next()){
					totalInfluence += rs.getInt("Influence");
				}
			}
			return totalInfluence;
		} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
			return -1;
		}
	}
	
	public static int getGuildMaxInfluence(String guildName){
		int totalInfluence = 0;
		
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT * FROM Guilds WHERE Title = '" + guildName + "'");
			if(rs.next()){
				rs = SQLManager.runSQLQuery("SELECT * FROM Players WHERE GuildID = '" + rs.getInt("id") + "'");
				while(rs.next()){
					totalInfluence += rs.getInt("MaxInfluence");
				}
			}
			return totalInfluence;
		} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
			return -1;
		}
	}
	
	public static int getGuildGroundsByName(String guildName) {
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT * FROM Guilds WHERE Title = '" + guildName + "'");
			if(rs.next()){
				return rs.getInt("Grounds");
			}
		} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
		}
		return -1;
	}
	
	public static int getGuildGroundsByID(int guildID) {
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT * FROM Guilds WHERE id = '" + guildID + "'");
			if(rs.next()){
				return rs.getInt("Grounds");
			}
		} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
		}
		return -1;
	}
	
	public static String getActiveMembers(String guild) {//TODO why
		List<String> membersList = new ArrayList<>();
		String membersString = "";
		
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT * FROM Guilds WHERE Title = '" + guild + "'");
			if(rs.next()){
				int guildID = rs.getInt("id");
				rs = SQLManager.runSQLQuery("SELECT * FROM Players WHERE GuildID = '" + guildID + "'");
				
				while(rs.next()){
					String username = rs.getString("Username");
					Player userPlayer;
					try{
					userPlayer = Sponge.getServer().getPlayer(username).get();
					}catch(Exception e){
						continue;
					}
					//Main.logger.debug(username, server.getOnlinePlayers().contains(userPlayer));
					if(Sponge.getServer().getOnlinePlayers().contains(userPlayer)){
						membersList.add(username);
					}
				}
				for(int i = 0; i < membersList.size(); i++){
					membersString += membersList.get(i);
					if(i != membersList.size()-1){
						membersString += ", ";
					}
				}
			}
			return membersString;
		} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
			return "A fault occured in collecting this information, please report this to an administrator.";
		}
	}
	
	public static String getDormantMembers(String guild) {//TODO bad
		List<String> membersList = new ArrayList<>();
		String membersString = "";
		
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT * FROM Guilds WHERE Title = '" + guild + "'");
			if(rs.next()){
				int guildID = rs.getInt("id");
				rs = SQLManager.runSQLQuery("SELECT * FROM Players WHERE GuildID = '" + guildID + "'");
				
				while(rs.next()){
					String username = rs.getString("Username");
					//Player userPlayer;//TODO why
					try{
						//userPlayer = Sponge.getServer().getPlayer(username).get();
					}catch(Exception e){
						membersList.add(username);
						continue;
					}
				}
				for(int i = 0; i < membersList.size(); i++){
					membersString += membersList.get(i);
					if(i != membersList.size()-1){
						membersString += ", ";
					}
				}
			}
			return membersString;
		} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
			return "A fault occured in collecting this information, please report this to an administrator.";
		}
	}

	public static String getGuildDescription(String guild) {
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT * FROM Guilds WHERE Title = '" + guild + "'");
			
			if(rs.next()){
				return rs.getString("Description");
			}else{
				return "Default description.";
			}
		} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
			return "Error";
		}
	}

	public static String getGuildName(Player arg0){
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT * FROM Players WHERE UUID = '" + arg0.getIdentifier() + "'");
			
			if(rs.next()){
				rs = SQLManager.runSQLQuery("SELECT * FROM Guilds WHERE id = '" + rs.getInt("GuildID") + "'");
			}
			
			if(rs.next()){
				return rs.getString("Title");
			}else{
				return "Error";//TODO throw an exception
			}
		} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
			return "A fault occured in collecting this information, please report this to an administrator.";
		}
	}
	
	public static boolean playerInGuild(UUID uuid) {
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT id FROM Players WHERE uuid = '" + uuid + "'");
			
			if(rs.next()){
				return true;
			}
			
			return false;
		} catch (SQLException e) {//TODO throw
			Guilds.logger.error("A database error has occured: \n" + e);
			return false;
		}
	}
	
	public static void setGuildDescription(int guildID, String description, boolean broadcast) {
		try {
			SQLManager.runSQLAction("UPDATE Guilds SET Description = '" + description + "' WHERE id ='" + guildID + "'");
			if(broadcast)
				Sponge.getServer().getBroadcastChannel().send(Text.of(getGuildNameByID(guildID) + " set their guild description to '" + description + "'"));//TODO Colour
			
		} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
		}
		
	}

	public static int getMemberGuildID(UUID memberUUID) {
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT * FROM Players WHERE UUID = '" + memberUUID + "'");
			if(rs.next()){
				return rs.getInt("GuildID");
			}
			return 1;//TODO throw
		} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
			
			return -1;
		}
	}
	
	public static String getGuildNameByID(int guildID) {
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT Title FROM Guilds WHERE id = '" + guildID + "'");
			if(rs.next()){
				return rs.getString("Title");
			}
			return "None";//TODO throw
		} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
			
			return "Error";
		}
	}
	
	public static Rank getMemberRankByUUID(UUID memberUUID) {
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT Rank FROM Players WHERE UUID = '" + memberUUID + "'");
			if(rs.next()){
				return Rank.values()[rs.getInt("Rank")];
			}
			
			return Rank.DEFAULT;
		} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
			return Rank.DEFAULT;
		}
	}
	
	public static Rank getMemberRankByName(String memberName) {
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT Rank FROM Players WHERE Username = '" + memberName + "'");
			if(rs.next()){
				return Rank.values()[rs.getInt("Rank")];
			}
			
			return Rank.DEFAULT;
		} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
			return Rank.DEFAULT;
		}
	}
	
	public static boolean guildHasFreeInfluence(int guildID, int landToClaim) {
		boolean result = false;
		int totalInfluence = 0;
		int totalGrounds = 0;
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT * FROM Players WHERE GuildID = '" + guildID + "'");
			while(rs.next()){
				totalInfluence += rs.getInt("Influence");
			}
			
			rs = SQLManager.runSQLQuery("SELECT * FROM Guilds WHERE id = '" + guildID + "'");
			if(rs.next()){
				totalGrounds = rs.getInt("Grounds");
				if((totalInfluence - totalGrounds - landToClaim) >= 0){
					return true;
				}
			}
		
			return result;
		} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
			return false;
		}
	}
	
	public static int getGuildInfluence(int GuildID){
		int totalInfluence = 0;
		
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT * FROM Players WHERE GuildID = '" + GuildID + "'");
			while(rs.next()){
				totalInfluence += rs.getInt("Influence");
			}
			
			return totalInfluence;
		} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
			return -1;
		}
	}
	
	public static int getGuildIDByTitle(String title){//TODO caps
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT id FROM Guilds WHERE Title = '" + title + "'");
			if(rs.next()){
				return rs.getInt("id");
			}
			
			return -1;
		} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
			return -1;
		}
	}
	
	public static boolean isGroundsFree(int xChunk, int zChunk, String world) {
		boolean result = false;
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT * FROM Grounds WHERE ChunkX = '" + xChunk + "' AND ChunkZ = '" + zChunk + "' AND World = '" + world + "'");
			if(rs.next()){
				int guildID = rs.getInt("GuildID");
				result = guildID != 1 && GuildsAPI.getGuildGroundsByID(guildID) > getGuildInfluence(guildID);
			}else{
				result = true;
			}
			
			return result;
		} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
			return false;
		}
	}
	
	public static int ordinateToChunkValue(int pos){
		int chunk = (int)pos/16;
		if(pos >= 0){
			chunk++;
	    }else{
	    	chunk--;
		}
		return chunk;
	}
	
	/***
	 * @param xChunk
	 * @param zChunk
	 * @param world World of claim
	 * @param p The player claiming
	 * @param guildID Claiming guild id
	 * @return Whether the claim succeeded
	 */
	public static boolean claimCheck(int xChunk, int zChunk, String world, Player p, int guildID){
		if (GuildsAPI.getMemberRankByUUID(p.getUniqueId()).value >= Rank.MOD.value){
			if(GuildsAPI.isGroundsFree(xChunk, zChunk, world)){
				if(GuildsAPI.guildHasFreeInfluence(guildID, 1)){
					return true;
				}else{
					p.sendMessage(Text.builder("You do not have enough influence to claim this land.").color(TextColors.RED).build());
					return false;
				}
			}else{
				p.sendMessage(Text.builder("This land is already securely claimed.").color(TextColors.RED).build());
				return false;
			}
		}else{
			p.sendMessage(Text.of(GuildsAPI.getMemberRankByUUID(p.getUniqueId())));
			p.sendMessage(Text.of(GuildsAPI.getMemberRankByUUID(p.getUniqueId()).value));
			p.sendMessage(Text.of(Rank.MOD.value));
			p.sendMessage(Text.builder("You must be at least a moderator of your guild to claim land.").color(TextColors.RED).build());
			return false;
		}
	}
	
	public static int getChunkOwnerID(int xChunk, int zChunk, String world){
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT * FROM Grounds WHERE ChunkX = '" + xChunk + "' AND ChunkZ = '" + zChunk + "' AND World = '" + world + "'");
			if(rs.next()){
			return rs.getInt("GuildID");
			}else{
				return 1; //default
			}
		} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
			return -1;
		}
	}
	
	public static void setMemberGuild(String memberName, int guildID) {
		try {
			SQLManager.runSQLAction("UPDATE Players SET GuildID = " + guildID + " WHERE Username ='" + memberName + "'");
		} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
		}
	}
	
	public static void setMemberGuild(UUID memberUUID, int guildID) {
		try {
			SQLManager.runSQLAction("UPDATE Players SET GuildID = " + guildID + " WHERE UUID ='" + memberUUID + "'");
		} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
		}
	}
	
	public static void sendGuildMessage(Text message, int id) {
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT * FROM Players WHERE GuildID = '" + id + "'");
			while (rs.next()) {
				Player member = Sponge.getServer().getPlayer(rs.getString("Username")).orElse(null);
				if (member != null) {
					member.sendMessage(message);
				}
			}
		} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
		}
	}
	
	public static void sendGuildMessageExclude(Text message, int id, String[] excludedMemberNames) {
		try {
			ResultSet rs = SQLManager.runSQLQuery("SELECT * FROM Players WHERE GuildID = '" + id + "'");
			while (rs.next()) {
				Player member = Sponge.getServer().getPlayer(rs.getString("Username")).orElse(null);
				if (member != null) {
					boolean contained = false;
					for(String n : excludedMemberNames){
						if(n.equals(member.getName())){
							contained = true; break;
						}
					}
					if (!contained) {
						member.sendMessage(message);
					}
				}
			}
		} catch (SQLException e) {
			Guilds.logger.error("A database error has occured: \n" + e);
		}
	}
}
