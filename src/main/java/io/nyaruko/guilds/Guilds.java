package io.nyaruko.guilds;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.spongepowered.api.Game;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.event.game.state.GameStoppingServerEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.text.Text;
import com.google.inject.Inject;
import io.nyaruko.guilds.commands.Claim;
import io.nyaruko.guilds.commands.CommandController;
import io.nyaruko.guilds.commands.Create;
import io.nyaruko.guilds.commands.Description;
import io.nyaruko.guilds.commands.Enemy;
import io.nyaruko.guilds.commands.Invite;
import io.nyaruko.guilds.commands.Join;
import io.nyaruko.guilds.commands.Leader;
import io.nyaruko.guilds.commands.Leave;
import io.nyaruko.guilds.commands.ListCommand;
import io.nyaruko.guilds.commands.MapCommand;
import io.nyaruko.guilds.commands.Mod;
import io.nyaruko.guilds.commands.Who;

import org.slf4j.Logger;
//TODO Language support
//TODO report errors to user
//TODO console handling
@Plugin(id = "guilds", name = "Guilds", version = "0.0.1")
public class Guilds {
	public static Guilds grab;
	private static String version = "0.0.1";
	@Inject
	public static Logger logger;
	@Inject
	public Game game;
	
	public static List<String> playerNames = new ArrayList<>();
	public static List<Integer> playerLastGuilds = new ArrayList<>();
	
	@Inject
	private void setLogger(Logger logger) {
	    Guilds.logger = logger;
	}
	
	@Listener
	public void onServerStart(GameStartedServerEvent event) {
		try{
			SQLManager.setupDatabase();
		}catch (SQLException e){
			logger.error("Database connection failed: \n" + e);
		}
		
		registerCommands();
		Sponge.getEventManager().registerListeners(this, new Events());
		logger.info("Guilds V" + version + " started successfully.");
	}
	
	private void registerCommands(){
		//TODO admin commands
		CommandSpec whoCmd = CommandSpec.builder()
			    .permission("guilds.user.who")
			    .description(Text.of("Get guild information about yourself."))
			    .executor(new Who())
			    .build();
		
		CommandSpec createCmd = CommandSpec.builder()
			    .permission("guilds.user.create")
			    .description(Text.of("Create a guild of your own."))
			    .arguments(GenericArguments.onlyOne(GenericArguments.string(Text.of("title"))))
			    .executor(new Create())
			    .build();
		
		CommandSpec inviteCmd = CommandSpec.builder()
			    .permission("guilds.user.invite")
			    .description(Text.of("Invite another player to join your guild."))
			    .arguments(GenericArguments.onlyOne(GenericArguments.player(Text.of("player"))))
			    .executor(new Invite())
			    .build();
		
		CommandSpec descriptionCmd = CommandSpec.builder()
			    .permission("guilds.user.description")
			    .description(Text.of("Set your guild description."))
			    .arguments(GenericArguments.remainingJoinedStrings((Text.of("description"))))
			    .executor(new Description())
			    .build();
		
		CommandSpec joinCmd = CommandSpec.builder()
			    .permission("guilds.user.join")
			    .description(Text.of("Join an existing guild."))
			    .arguments(GenericArguments.onlyOne(GenericArguments.string(Text.of("title"))))
			    .executor(new Join())
			    .build();
		
		CommandSpec leaveCmd = CommandSpec.builder()
			    .permission("guilds.user.leave")
			    .description(Text.of("Leave your current guild."))
			    .executor(new Leave())
			    .build();
		
		Map<String, String> shapes = new HashMap<String, String>();	
		shapes.put("s", "square");
		shapes.put("square", "square");
		shapes.put("c", "circle");
		shapes.put("circle", "circle");
		CommandSpec claimCmd = CommandSpec.builder()
			    .permission("guilds.user.claim")
			    .description(Text.of("Claim the chunk you are currently standing in."))
			    .arguments(GenericArguments.optionalWeak(GenericArguments.seq(//TODO additional perms
			    		GenericArguments.choices(Text.of("shape"), shapes),
			    		GenericArguments.integer(Text.of("radius")))))
			    .executor(new Claim())
			    .build();
		
		CommandSpec mapCmd = CommandSpec.builder()
			    .permission("guilds.user.map")
			    .description(Text.of("Display a map of nearby grounds and which guilds claim them."))
			    .executor(new MapCommand())
			    .build();
		
		CommandSpec listCmd = CommandSpec.builder()
			    .permission("guilds.user.list")
			    .description(Text.of("Display a list of guilds in order of online players."))
			    .arguments(GenericArguments.onlyOne(GenericArguments.optional(GenericArguments.integer(Text.of("page")))))
			    .executor(new ListCommand())
			    .build();

		CommandSpec modCmd = CommandSpec.builder()
			    .permission("guilds.user.mod")
			    .description(Text.of("Change the rank of a member of your guild to a moderator."))
			    .arguments(GenericArguments.optionalWeak(GenericArguments.player(Text.of("player"))), GenericArguments.optional(GenericArguments.string(Text.of("playerStr"))))
			    .executor(new Mod())
			    .build();
		
		CommandSpec leaderCmd = CommandSpec.builder()
			    .permission("guilds.user.leader")
			    .description(Text.of("Hand leadership of your guild over to another member."))
			    .arguments(GenericArguments.optionalWeak(GenericArguments.player(Text.of("player"))), GenericArguments.optional(GenericArguments.string(Text.of("playerStr"))))
			    .executor(new Leader())
			    .build();
		
		CommandSpec enemyCmd = CommandSpec.builder()
			    .permission("guilds.user.enemy")
			    .description(Text.of("Declare another guild to be an enemy of your guild."))
			    .arguments(GenericArguments.onlyOne(GenericArguments.string(Text.of("title"))))
			    .executor(new Enemy())
			    .build();
		
		CommandSpec myCommandSpec = CommandSpec.builder()
				.description(Text.of("Guilds Command"))
				.permission("guilds.user.help")
				.child(whoCmd, "who")
				.child(createCmd, "create")
				.child(inviteCmd, "invite", "inv", "i")
				.child(descriptionCmd, "description", "desc")
				.child(joinCmd, "join")
				.child(leaveCmd, "leave", "quit")
				.child(claimCmd, "claim")
				.child(mapCmd, "map")
				.child(listCmd, "list", "ls", "l")
				.child(modCmd, "mod", "officer")
				.child(leaderCmd, "leader")
				.child(enemyCmd, "enemy")
				.executor(new CommandController())
				.build();
		
		Sponge.getCommandManager().register(this, myCommandSpec, "guild", "guilds", "g");
	}
	
	@Listener
	public void onServerStop(GameStoppingServerEvent event){
		try {
			SQLManager.con.close();
		} catch (SQLException e) {
			logger.error("Failed to properly close database connection: \n" + e);
		}
	}
}
