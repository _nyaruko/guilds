package io.nyaruko.guilds;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.data.Transaction;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.cause.Cause;
import org.spongepowered.api.event.entity.MoveEntityEvent;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class Events {
	
	@Listener
	public void onMove(MoveEntityEvent event){
		if(event.getTargetEntity() instanceof Player) {
			Player p = (Player)event.getTargetEntity();
			int newX = event.getToTransform().getLocation().getBlockX();
			int newZ = event.getToTransform().getLocation().getBlockZ();
			
			if(event.getFromTransform().getLocation().getBlockX() == newX && event.getFromTransform().getLocation().getBlockZ() == newZ){
				return;
			}
			
			String name = p.getName();//TODO consider display names
			//get chunk pos
			int ChunkX = (int)newX/16;
			int ChunkZ = (int)newZ/16;
			
			if(newX >= 0){
				ChunkX++;
			}else{
				ChunkX--;
			}
			
			if(newZ >= 0){
				ChunkZ++;
			}else{
				ChunkZ--;
			}
			
			try {
				String World = p.getWorld().getUniqueId().toString();
				ResultSet rs = SQLManager.runSQLQuery("SELECT * FROM Grounds WHERE ChunkX = '" + ChunkX + "' AND ChunkZ = '" + ChunkZ + "' AND World = '" + World + "'");
				if(rs.next()){
					rs = SQLManager.runSQLQuery("SELECT * FROM Guilds WHERE id = '" + rs.getString("GuildID") + "'");
				}
				
				for (int i = 0; i < Guilds.playerNames.size(); i++){//locate player in array
					if(Guilds.playerNames.get(i).equals(name)){
						if(rs.next()){//are the grounds claimed
							if(Guilds.playerLastGuilds.get(i) == rs.getInt("id")){//if players previous chunk was claimed by same function do not do anything
								break;
							}else{
								Guilds.playerLastGuilds.set(i, rs.getInt("id"));
								p.sendMessage(Text.builder(rs.getString("Title")).color(GuildsAPI.getGuildRelationColor(name, rs.getInt("id")))
													.append(Text.builder(" - ").color(TextColors.GOLD)
													.append(Text.builder(rs.getString("Description")).color(TextColors.WHITE)
													.build()).build()).build());
							}
						}else{//if not check if player was already in the area
							if(Guilds.playerLastGuilds.get(i) == 1){
								break;
							}else{
								Guilds.playerLastGuilds.set(i, 1);
								String[] objects = GuildsAPI.getDefaultGuildData();
								p.sendMessage(Text.builder(objects[0]).color(GuildsAPI.getGuildRelationColor(name, 1))
													.append(Text.builder(" - ").color(TextColors.GOLD)
													.append(Text.builder(objects[1]).color(TextColors.WHITE)
													.build()).build()).build());//guild area change
							}
						}
					}
				}
				
				
			} catch (Exception e) {
				Guilds.logger.error("Error getting player position: \n" + e);
			}
		}
	}
	
	@Listener
	public void onBlockChange(ChangeBlockEvent event){
		Cause cause = event.getCause();
		Player p = null;
		if (cause.containsType(Player.class)){//TODO compare to a version with @Root Player p
			Optional<Player> op = cause.first(Player.class);
			if(op.isPresent()){
				p = op.get();
			}
		}
		if(p != null) {
			for(Transaction<BlockSnapshot> block: event.getTransactions()){
				int xPos = block.getOriginal().getLocation().get().getBlockX();
				int zPos = block.getOriginal().getLocation().get().getBlockZ();
				
				int xChunk = (int)xPos/16;
				int zChunk = (int)zPos/16;
				if(xPos >= 0){
					   xChunk++;
					  }else{
					   xChunk--;
					  }
				if(zPos >= 0){
					   zChunk++;
					  }else{
					   zChunk--;
				}
				
				try {
					String World = p.getWorld().getUniqueId().toString();
					ResultSet rs = SQLManager.runSQLQuery("SELECT * FROM Grounds WHERE ChunkX = '" + xChunk + "' AND CHUNKZ = '" + zChunk + "' AND World = '" + World + "'");
					if(rs.next()){
						int landGuildID = rs.getInt("GuildID");
						rs = SQLManager.runSQLQuery("SELECT * FROM Players WHERE Username = '" + p.getName() + "'");
						int playerGuildID = 1;
						if(rs.next()){
							playerGuildID = rs.getInt("GuildID");
						}
						if(playerGuildID != landGuildID && landGuildID != 1){
							p.sendMessage(Text.of("You cannot build in land that is claimed by TODOLOADNAME"));//TODO limit rate
							event.setCancelled(true);
						}
					}else{
						
					}
				} catch (SQLException e) {
					Guilds.logger.error("A database error has occured: \n" + e);
				}
			}
		}
	}
	
	@Listener
	public void onPlayerJoin(ClientConnectionEvent.Join event) throws SQLException {
		Player p = event.getTargetEntity();
		String username = p.getName();
		String uuid = p.getIdentifier();
		
		int userArrayPosition = 0;
		boolean isInList = false;
		for (int i = 0; i < Guilds.playerNames.size(); i++){//TODO look at contains
			if(Guilds.playerNames.get(i).equals(username)){
				isInList = true;
				userArrayPosition = i;
				break;
			}
		}
		if(!isInList){
			Guilds.playerNames.add(username);
			Guilds.playerLastGuilds.add(1);
			userArrayPosition = Guilds.playerNames.size()-1;
			//Main.logger.info("Inserted user at " +  (playerNames.size()-1));
		}
		ResultSet rs = SQLManager.runSQLQuery("SELECT * FROM Players WHERE UUID = '" + uuid + "'");
		
		if(rs.next()){
			if(!rs.getString("Username").equals(username)){
				String oldName = rs.getString("Username");
				SQLManager.runSQLAction("UPDATE Players SET Username = '" + username + "' WHERE UUID ='" + uuid + "'");
				Guilds.logger.info("Username changed! UUID " + uuid + " changed username from " + oldName + " to " + username);
			}
		}else{
			SQLManager.runSQLAction("INSERT INTO Players (UUID, Username, GuildID, MaxInfluence, Influence) VALUES ('" + uuid + "', '" + username + "', '1', '10', '0')");
			Guilds.logger.info("Added " + username + " to the player list!");
			//TODO make influence start and max variable
		}
		
		int newX = p.getLocation().getBlockX();
		int newZ = p.getLocation().getBlockZ();
		int ChunkX = (int)newX/16;
		int ChunkZ = (int)newZ/16;
		
		String World = p.getWorld().getUniqueId().toString();
		rs = SQLManager.runSQLQuery("SELECT * FROM Grounds WHERE ChunkX = '" + ChunkX + "' AND ChunkZ = '" + ChunkZ + "' AND World = '" + World + "'");
		if(rs.next()){
			Guilds.playerLastGuilds.set(userArrayPosition, rs.getInt("GuildID"));
		}else{
			Guilds.playerLastGuilds.set(userArrayPosition, 1);
		}
	}
}
